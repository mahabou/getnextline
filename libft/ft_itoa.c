/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maabou-h <maabou-h@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 15:32:05 by maabou-h          #+#    #+#             */
/*   Updated: 2018/11/10 12:48:43 by maabou-h         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

static int	ft_intlen(int n)
{
	int intlen;

	intlen = 1;
	if (n == -2147483648)
		return (11);
	if (n < 0)
	{
		n = -n;
		intlen++;
	}
	while (n > 9)
	{
		intlen++;
		n /= 10;
	}
	return (intlen);
}

char		*ft_itoa(int n)
{
	char	*itoa;
	long	nbr;
	int		end;

	if (!(itoa = ft_strnew(ft_intlen(n))))
		return (NULL);
	nbr = n;
	end = ft_intlen(n);
	itoa[end--] = '\0';
	if (n < 0)
	{
		nbr = -nbr;
		itoa[0] = '-';
	}
	if (n == 0)
		*itoa = '0';
	while (nbr)
	{
		itoa[end--] = nbr % 10 + '0';
		nbr /= 10;
	}
	return (itoa);
}
