/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maabou-h <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/08 16:21:59 by maabou-h          #+#    #+#             */
/*   Updated: 2018/11/12 21:33:49 by maabou-h         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

static void	ft_strlowcase(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] >= 65 && str[i] <= 90)
			str[i] += 32;
		i++;
	}
}

static int	alphacheck(char str)
{
	if (str >= 'a' && str <= 'z')
		return (1);
	else
		return (0);
}

static int	numcheck(char str)
{
	if (str >= '0' && str <= '9')
		return (1);
	else
		return (0);
}

char		*ft_strcapitalize(char *str)
{
	int i;

	i = 0;
	ft_strlowcase(str);
	while (str[i] != '\0')
	{
		if (alphacheck(str[i]) == 1 && numcheck(str[i]) == 0 &&
				alphacheck(str[i - 1]) == 0 && numcheck(str[i - 1]) == 0)
		{
			str[i] -= 32;
			i++;
		}
		i++;
	}
	return (str);
}
